// auth-service.js
const DurationCalculator = (duration) => {
    const hour = (duration - (duration % 3600)) / 3600;
    const minutes = ((duration % 3600) - (duration % 60)) / 60;
    const sec = duration % 60;
    let durationStr = '';
    if(hour>0){
        durationStr = `${hour}h `;
    }
    if(minutes>0){
        durationStr = durationStr + `${minutes}m `;
    }
    durationStr = durationStr + `${sec}s`;
    return durationStr;
};
export default DurationCalculator;