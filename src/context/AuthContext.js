import { createContext, useContext } from 'react';

const AuthContext = createContext(false);

export function useAuth() {
    return useContext(AuthContext);
}

export default AuthContext;