import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import PlaylistPlayIcon from '@material-ui/icons/PlaylistPlay';
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';
import {
    Link
} from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    card: {
        maxWidth: 345,
    },
    media: {
        height: 0,
        paddingTop: '100%', // square image
    },
    avatar: {
        backgroundColor: red[500],
    },
}));

export default function SingleCoverAndName(props) {
    const classes = useStyles();

    return (
        <Card className={classes.card}>
            <CardMedia
                className={classes.media}
                image={props.imgCover}
                title="Cover"
            />
            <CardContent>
                <Typography color="textSecondary" component="h5">
                    {props.name}
                </Typography>
            </CardContent>
            <CardActions disableSpacing>
                <IconButton aria-label="add to favorites">
                    <FavoriteIcon color="secondary" />
                </IconButton>
                <IconButton aria-label="share">
                    <Link to={props.linkToSinglePage}>
                        <PlaylistPlayIcon color="action"/>
                    </Link>
                </IconButton>
                <IconButton aria-label="share">
                    <PlayCircleOutlineIcon color="action"/>
                </IconButton>
            </CardActions>
        </Card>
    );
}