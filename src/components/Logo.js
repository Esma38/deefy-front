import React from 'react';
import Logo from '../Images/_Logo.png';

function DeefyLogo(props) {
    return (
        <img src={Logo} alt={"Deefy-logo"} className={"logo"}/>
    );
}

export default DeefyLogo;