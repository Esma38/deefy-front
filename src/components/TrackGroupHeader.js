import React from 'react';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    paper: {
      marginTop: '40px',
        padding: '20px',
    },
    cover: {
        width: '100%',
        height: 'auto',
    },
}));
function TrackGroupHeader(props) {

    const classes = useStyles();
    return (
        <Container>
            <Paper className={classes.paper}>
                <Grid container spacing={3}>
                    <Grid item xs={12} sm={4}>
                        <img className={classes.cover} src={props.coverPath} alt={"playlist cover"}/>
                    </Grid>
                    <Grid item xs={12} sm={8}>
                        <h1>
                            {props.name}
                        </h1>
                        <p>Nbr de titres : {props.trackNbr}</p>
                        <p>Durée : {props.duration}</p>
                    </Grid>
                </Grid>
            </Paper>
        </Container>
    );
}

export default TrackGroupHeader;