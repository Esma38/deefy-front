import React from 'react';
import BgRegister from '../Images/fondRegister.jpg';

function bgRegister(props) {
    return (
        <img src={BgRegister} alt={"fond"} style={{width:'100%', height: '100%'}} className={"BgRegister"}/>
    );
}

export default bgRegister;