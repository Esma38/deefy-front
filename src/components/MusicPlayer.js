import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import CardMedia from '@material-ui/core/CardMedia';


const useStyles = makeStyles(theme => ({
    main: {
        marginTop: theme.spacing(8),
        marginBottom: theme.spacing(2),
    },
    footer: {
        padding: theme.spacing(3, 2),
        marginTop: 'auto',
        backgroundColor:
            theme.palette.type === 'dark' ? theme.palette.grey[800] : theme.palette.grey[200],
    },
    details: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    content: {
        flex: '1 0 auto',
    },
    cover: {
    },
    controls: {
        display: 'flex',
        alignItems: 'center',
        flexFlow: 'column',
        paddingLeft: theme.spacing(1),
        paddingBottom: theme.spacing(1),
    },
    playIcon: {
        height: 38,
        width: 38,
    },
    volume: {
        width: 200,
    },
}));

export default function MusicPlayer(props) {
    const classes = useStyles();
    const [value, setValue] = React.useState(30);

    //TODO do something with the sound information
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    let title = '';
    let artistName = '';
    if (props.musicToPlay !== undefined){
        title = props.musicToPlay.data[0].title;
        artistName = props.musicToPlay.data[0].artiste[0].nom;
    }

    return (
        <footer className={`${classes.footer} musicPlayer`}>
            <Container>
                <div className={classes.details}>
                    <div>
                        <Typography component="h5" variant="h5">
                            {title}
                        </Typography>
                        <Typography variant="subtitle1" color="textSecondary">
                            {artistName}
                        </Typography>
                    </div>
                    <figure>
                        <audio
                        controls
                        src={props.musicToPlay.data.fullMusicPath}>
                        <code>audio</code>
                        </audio>
                        </figure>
                </div>
                <CardMedia
                    className={classes.cover}
                    image="/static/images/cards/live-from-space.jpg"
                    title="Live from space album cover"
                />
            </Container>
        </footer>
    );
}