import React from 'react';
import useAxios from 'axios-hooks';
import TrackList from "../components/TrackList";
import TrackGroupHeader from "../components/TrackGroupHeader";
import {
    useParams
} from "react-router-dom";
import DurationCalculator from "../service/DurationCalculator";

export default function ArtistPage(props) {
    let { artistId } = useParams();
    //axios hooks request on the api
    const [{ data, loading, error }] = useAxios(
        `http://localhost:8181/api/artists/${artistId}`
    );
    if (loading) return(
        <>

        </>
        );
    if (error) return <p>Error!</p>;
    let totalSecDuration = 0;
    data[0].musique.map((musique) => {
            return totalSecDuration = totalSecDuration + parseInt(musique.duration);
    });
    return (
        <>
            <TrackGroupHeader coverPath={data[0].fullCoverPath} name={`Artiste : ${data[0].nom}`} trackNbr={data[0].musique.length} duration={DurationCalculator(totalSecDuration)}/>
            <TrackList rows={data[0].musique} playNewSong={props.playNewSong}/>
        </>
    );
}