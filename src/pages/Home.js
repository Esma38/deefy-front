import React from 'react';
import Container from "@material-ui/core/Container";
import SingleCoverAndName from "../components/SingleCoverAndName";
import Grid from "@material-ui/core/Grid";
import useAxios from "axios-hooks";

function Home (props) {
    const [{ data, loading, error }] = useAxios(
        "http://localhost:8181/api/last-albums"
    );
    if (loading) return(
        <>

        </>
    );
    if (error) return <p>Error!</p>;

    return(
        <>
            <Container className={'marginTop'}>
                <h3>
                    Découvrez les derniers albums
                </h3>
                <Grid container spacing={3}>
                    {
                        data.map((album, index) => {
                            return (
                                <Grid item xs={6} sm={3} key={index}>
                                    <SingleCoverAndName linkToSinglePage={`/album/${album.id}`} name={album.title} imgCover={album.fullCoverPath}/>
                                </Grid>
                            )
                        })
                    }
                </Grid>
            </Container>
        </>
    )
}
export default Home;