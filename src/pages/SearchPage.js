import React from 'react';
import TrackList from "../components/TrackList";
import Container from "@material-ui/core/Container";

export default function SearchPage (props) {

    return(
        <>
            <Container>
                <TrackList title={props.title}/>
            </Container>
        </>
    )
}