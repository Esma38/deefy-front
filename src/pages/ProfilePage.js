import React from 'react';
import {useParams} from "react-router-dom";
import useAxios from "axios-hooks";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import SingleCoverAndName from "../components/SingleCoverAndName";
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            {value === index && <Box p={3}>{children}</Box>}
        </Typography>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}

const useStyles = makeStyles(theme => ({
    root: {
        backgroundColor: theme.palette.background.paper
    },
}));

export default function ProfilePage (props) {
    const classes = useStyles();
    const theme = useTheme();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleChangeIndex = index => {
        setValue(index);
    };
    let { userId } = useParams();
    //axios hooks request on the api
    const [{ data, loading, error }] = useAxios(
        `http://localhost:8181/api/users/${userId}`
    );
    if (loading) return(
        <>

        </>
    );
    if (error) return <p>Error!</p>;

    return(
        <Container>
            <h1>
                Profil de : {data[0].firstname}
            </h1>
            <div className={'marginTop'}>
                <div className={classes.root}>
                    <AppBar position="static" color="default">
                        <Tabs
                            value={value}
                            onChange={handleChange}
                            indicatorColor="primary"
                            textColor="primary"
                            variant="fullWidth"
                            aria-label="full width tabs example"
                        >
                            <Tab label="Playlists" {...a11yProps(0)} />
                            <Tab label="Informations" {...a11yProps(1)} />
                        </Tabs>
                    </AppBar>
                    <SwipeableViews
                        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                        index={value}
                        onChangeIndex={handleChangeIndex}
                    >
                        <TabPanel value={value} index={0} dir={theme.direction}>
                            <Grid container spacing={3}>
                                {data[0].playlist.map((playlist, index) => {
                                        return (
                                            <Grid item xs={6} sm={3} key={index}>
                                                <SingleCoverAndName linkToSinglePage={`/playlist/${playlist.id}`} name={playlist.name} imgCover={playlist.fullCoverPath}/>
                                            </Grid>
                                        );
                                    }
                                )}
                            </Grid>
                        </TabPanel>
                        <TabPanel value={value} index={1} dir={theme.direction}>
                            <p>
                                Nom : {data[0].name}
                            </p>
                            <p>
                                Prénom : {data[0].firstname}
                            </p>
                            <p>
                                Email : {data[0].email}
                            </p>
                            <p>
                                Avatar : <br/>
                                <img src={data[0].fullPathPlayListCover} alt={"user avatar"}/>
                            </p>
                        </TabPanel>
                    </SwipeableViews>
                </div>
            </div>
        </Container>
    )
}