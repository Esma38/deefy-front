import React, {useState, useEffect} from 'react';
import './App.scss';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from 'react-router-dom';
import Home from "./pages/Home";
import ProfilePage from "./pages/ProfilePage";
import PlaylistPage from "./pages/PlaylistPage";
import SearchPage from "./pages/SearchPage";
import LoginPage from "./pages/LoginPage";
import RegisterPage from "./pages/RegisterPage";
import AuthContext from "./context/AuthContext";
import AlbumPage from "./pages/AlbumPage";
import ArtistPage from "./pages/ArtistPage";
import MusicPlayer from "./components/MusicPlayer";
import PrimaryMenu from "./components/Menu";
const axios = require('axios');

function App (props) {
    const [authTokens, setAuthTokens] = useState(true);
    const setTokens = (data) => {
        localStorage.setItem("tokens", JSON.stringify(data));
        setAuthTokens(data);
    };
    const [idMusicsToPlay, setIdMusicsToPlay] = useState(85);
    const [musicsToPlay, setMusicsToPlay] = useState({
        'data':{
            '0': {
                'artiste': {
                    '0' :  {
                        'nom': ' ',
                    }
                },
                'title': ' '
            }
        }
    });

    useEffect(() => {
        // Make a request for a user with a given ID
        axios.get(`http://localhost:8181/api/musiques/${idMusicsToPlay}`)
            .then(function (response) {
                // handle success
                setMusicsToPlay(response);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .finally(function () {
                // always executed
            });
    }, [idMusicsToPlay]);

    const playNewSong = (newSong) => {
        setIdMusicsToPlay(newSong)
    };

    return(
        <AuthContext.Provider value={{ authTokens, setAuthTokens: setTokens }}>
            <Router>
                <div className="App">
                    <PrimaryMenu/>
                    <Switch>
                        <Route path={"/profile/:userId"}>
                            <ProfilePage/>
                        </Route>
                        <Route path={"/album/:albumId"}>
                            <AlbumPage playNewSong={playNewSong}/>
                        </Route>
                        <Route path={"/artist/:artistId"}>
                            <ArtistPage playNewSong={playNewSong}/>
                        </Route>
                        <Route path={"/playlist/:playlistId"}>
                            <PlaylistPage playNewSong={playNewSong}/>
                        </Route>
                        <Route path={"/search"}>
                            <SearchPage title={"Brol"}/>
                        </Route>
                        <Route path={"/login"}>
                            <LoginPage/>
                        </Route>
                        <Route path={"/register"}>
                            <RegisterPage/>
                        </Route>
                        <Route path={"/"}>
                            <Home/>
                        </Route>
                    </Switch>
                    <MusicPlayer musicToPlay={musicsToPlay}/>
                </div>
            </Router>
        </AuthContext.Provider>
    )
}

export default App;
